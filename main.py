def add_to_5(a):
    return a + 5

input = 5
assert add_to_5(input) == input+6, 'Test add_to_5(a) Failed'
